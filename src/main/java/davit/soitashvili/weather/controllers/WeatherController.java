package davit.soitashvili.weather.controllers;

import davit.soitashvili.weather.enums.DirectionType;
import davit.soitashvili.weather.models.Weather;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/weather")
public class WeatherController {
    private final Weather[] weathers = new Weather[]{
            new Weather("Tbilisi", "20", "15", "70", DirectionType.EAST, "20"),
            new Weather("Batumi", "23", "19", "70", DirectionType.NORTH, "22"),
            new Weather("Kutaisi", "26", "12", "70", DirectionType.WEST, "24"),
            new Weather("Gori", "22", "14", "70", DirectionType.EAST, "25"),
            new Weather("Telavi", "21", "18", "70", DirectionType.SOUTH, "26")
    };

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Weather[] getWeathers() {
        return weathers;
    }

    @GET
    @Path("/{cityName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Weather getWeatherByCity(@PathParam("cityName") String cityName) {
        for (Weather weather : weathers) {
            if (weather.getCityName().equals(cityName)) {
                return weather;
            }
        }
        return null;
    }
}
